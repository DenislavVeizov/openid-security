## Keycloak Security / OpenID


Configuration Properties (application.yml):
	
	keycloak:
      client:
        id: (required)
        secret: (required)
        registration-id: (optional)
        scope: (optional)
        authorization-grant-type: (optional)
        app-base-uri: (optional)
        redirect-uri: (optional)
      provider:
        base-uri: (required)
        realm: (required)
        load-well-known-config: (optional)

	

Property Description
	
	keycloak.client.id = Client identifier from keycloak server
	keycloak.client.secret = Client secret from keycloak server
	keycloak.client.registration-id = The ID that uniquely identifies the ClientRegistration. It is optional and default value is clientID from configuration file !
	keycloak.client.scope = The scope(s) requested by the client during the Authorization Request flow, such as openid, email, or profile. Scopes have to be separated by comma. Default: "openid", "profile", "email", "address", "phone" 
	keycloak.client.authorization-grant-type = The OAuth 2.0 Authorization Framework defines four Authorization Grant types. The supported values are authorization_code, implicit, and client_credentials. Default: authorization_code
	keycloak.client.app-base-uri = Client application base uri. This property is required if keycloak.client.redirect-uri is not specified !
	keycloak.client.redirect-uri = The client’s registered redirect URI that the Authorization Server redirects the end-user’s user-agent to after the end-user has authenticated and authorized access to the client. This property is required if keycloak.client.app-base-uri is not specified ! 
	
	keycloak.provider.base-uri = Keycloak server base URI
	keycloak.provider.realm = Keycloak realm
	keycloak.provider.load-well-known-config = It is boolean property and if it is true, /.well-known json config will be used for client registration bean configuration. Default: true
	

Example config:
	
	keycloak:
      client:
        id: osim-portal
        secret: 205fba13-4800-4658-ac1b-772607504910
        app-base-uri: http://localhost:8085/rpo-portal
      provider:
        base-uri: http://localhost:8089
        realm: example-realm
	
