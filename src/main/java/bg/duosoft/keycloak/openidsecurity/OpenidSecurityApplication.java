package bg.duosoft.keycloak.openidsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenidSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenidSecurityApplication.class, args);
    }

}
