package bg.duosoft.keycloak.openidsecurity.config;

import bg.duosoft.keycloak.openidsecurity.config.logout.CustomLogoutHandler;
import bg.duosoft.keycloak.openidsecurity.config.token.AccessTokenRequest;
import bg.duosoft.keycloak.openidsecurity.config.user.CustomOidcUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.SessionManagementConfigurer;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.oauth2.client.oidc.web.logout.OidcClientInitiatedLogoutSuccessHandler;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.session.HttpSessionEventPublisher;


//    https://docs.spring.io/spring-security/site/docs/5.1.8.RELEASE/reference/html/jc.html
@Slf4j
@Order(10)
@Configuration
@EnableWebSecurity
public class DefaultOAuth2SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable();

        http
                .authorizeRequests()
                .antMatchers("/assets/**", "/css/**", "/js/**", "/img/**", "/images/**", "/fonts/**", "/public/**", "/webjars/**").permitAll()
                .anyRequest().authenticated();

        http
                .oauth2Login()
                .tokenEndpoint()
                .accessTokenResponseClient(AccessTokenRequest.customAccessTokenResponseClient());

        http
                .oauth2Login()
                .userInfoEndpoint()
                .oidcUserService(new CustomOidcUserService());

        http
                .sessionManagement()
                .sessionConcurrency(configurer -> configurer.expiredUrl("/"))
                .maximumSessions(-1)
                .sessionRegistry(sessionRegistry());

        http
                .logout()
                .logoutSuccessHandler(oidcLogoutSuccessHandler())
                .addLogoutHandler(CustomLogoutHandler.getLogoutHandler(sessionRegistry()));
    }

    private LogoutSuccessHandler oidcLogoutSuccessHandler() {
        OidcClientInitiatedLogoutSuccessHandler oidcLogoutSuccessHandler =
                new OidcClientInitiatedLogoutSuccessHandler(clientRegistrationRepository);
        // Sets the location that the End-User's User Agent will be redirected to
        // after the logout has been performed at the Provider
        oidcLogoutSuccessHandler.setPostLogoutRedirectUri("{baseUrl}");
        return oidcLogoutSuccessHandler;
    }

}

