package bg.duosoft.keycloak.openidsecurity.config.client;

import bg.duosoft.keycloak.openidsecurity.properties.SecurityPropertyAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.ClientRegistrations;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.oidc.IdTokenClaimNames;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Configuration
public class ClientRegistrationConfig {

    private static final String DEFAULT_CLIENT_REDIRECT_URI_PATTERN = "{baseUrl}/login/oauth2/code/{registrationId}";
    private static final String KEYCLOAK_AUTHORIZATION_URI_PATTERN = "{providerBaseUri}/auth/realms/{realmName}/protocol/openid-connect/auth";
    private static final String KEYCLOAK_TOKEN_URI_PATTERN = "{providerBaseUri}/auth/realms/{realmName}/protocol/openid-connect/token";
    private static final String KEYCLOAK_USER_INFO_URI_PATTERN = "{providerBaseUri}/auth/realms/{realmName}/protocol/openid-connect/userinfo";
    private static final String KEYCLOAK_JWKS_URI_PATTERN = "{providerBaseUri}/auth/realms/{realmName}/protocol/openid-connect/certs";
    private static final String KEYCLOAK_END_SESSION_ENDPOINT_PATTERN = "{providerBaseUri}/auth/realms/{realmName}/protocol/openid-connect/logout";
    private static final String KEYCLOAK_REVOCATION_ENDPOINT_PATTERN = "{providerBaseUri}/auth/realms/{realmName}/protocol/openid-connect/revoke";
    private static final String KEYCLOAK_INTROSPECTION_ENDPOINT_PATTERN = "{providerBaseUri}/auth/realms/{realmName}/protocol/openid-connect/token/introspect";
    private static final String KEYCLOAK_REGISTRATION_ENDPOINT_PATTERN = "{providerBaseUri}/auth/realms/{realmName}/clients-registrations/openid-connect";
    private static final String KEYCLOAK_CHECK_SESSION_IFRAME_PATTERN = "{providerBaseUri}/auth/realms/{realmName}/protocol/openid-connect/login-status-iframe.html";
    private static final String KEYCLOAK_ISSUER_URI_PATTERN = "{providerBaseUri}/auth/realms/{realmName}";

    @Autowired
    private SecurityPropertyAccess securityPropertyAccess;

    //Override default clientRegistrationRepository bean
    @Bean
    @DependsOn("securityPropertyAccess")
    public ClientRegistrationRepository clientRegistrationRepository() {
        return new InMemoryClientRegistrationRepository(this.clientRegistration());
    }

    private ClientRegistration clientRegistration() {
        boolean loadWellKnownConfig = securityPropertyAccess.getKeycloakProviderLoadWellKnownConfig();
        if (loadWellKnownConfig) {
            return buildFromOidcIssuerLocation();
        } else {
            return buildWithRegistrationId();
        }
    }

    private ClientRegistration buildFromOidcIssuerLocation() {
        return ClientRegistrations
                .fromOidcIssuerLocation(getIssuerUri())
                .registrationId(getRegistrationId())
                .clientId(securityPropertyAccess.getKeycloakClientId(true))
                .clientSecret(securityPropertyAccess.getKeycloakClientSecret(true))
                .authorizationGrantType(getAuthorizationGrantType())
                .scope(getScope())
                .redirectUri(getRedirectUri())
                .build();
    }

    private String[] getScope() {
        String[] propScope = securityPropertyAccess.getKeycloakClientScope();
        if (Objects.isNull(propScope) || propScope.length < 1) {
            return new String[]{"openid", "profile", "email", "address", "phone"};// Default scopes
        }

        return securityPropertyAccess.getKeycloakClientScope();
    }

    private ClientRegistration buildWithRegistrationId() {
        ClientRegistration clientRegistration = ClientRegistration.withRegistrationId(getRegistrationId())
                .clientId(securityPropertyAccess.getKeycloakClientId(true))
                .clientSecret(securityPropertyAccess.getKeycloakClientSecret(true))
                .authorizationGrantType(getAuthorizationGrantType())
                .redirectUri(getRedirectUri())
                .scope(getScope())
                .authorizationUri(getAuthorizationUri())
                .tokenUri(getTokenUri())
                .userInfoUri(getUserInfoUri())
                .userNameAttributeName(IdTokenClaimNames.SUB)
                .jwkSetUri(getJwksSetUri())
                .build();

        Map<String, Object> configurationMetadata = new HashMap<>();
        configurationMetadata.put("end_session_endpoint", getEndSessionEndpoint());
        configurationMetadata.put("revocation_endpoint", getRevocationEndpoint());
        configurationMetadata.put("introspection_endpoint", getIntrospectionEndpoint());
        configurationMetadata.put("check_session_iframe", getCheckSessionIframe());
        configurationMetadata.put("registration_endpoint", getRegistrationEndpoint());
        configurationMetadata.put("backchannel_logout_supported", true);
        configurationMetadata.put("backchannel_logout_session_supported", true);

        return ClientRegistration.withClientRegistration(clientRegistration)
                .providerConfigurationMetadata(configurationMetadata)
                .build();
    }

    private AuthorizationGrantType getAuthorizationGrantType() {
        String propGrantType = securityPropertyAccess.getKeycloakClientAuthorizationGrantType();
        if (StringUtils.hasText(propGrantType)) {
            return new AuthorizationGrantType(propGrantType);
        }
        return AuthorizationGrantType.AUTHORIZATION_CODE;// Default grant type
    }

    private String getRegistrationId() {
        String propRegistrationId = securityPropertyAccess.getKeycloakClientRegistrationId();
        if (StringUtils.hasText(propRegistrationId)) {
            return propRegistrationId;
        }

        return securityPropertyAccess.getKeycloakClientId(true);// By default client id will be used for registration id
    }

    private String getRedirectUri() {
        String propRedirectUri = securityPropertyAccess.getKeycloakClientRedirectUri();
        if (StringUtils.hasText(propRedirectUri)) {
            return propRedirectUri;
        }

        return DEFAULT_CLIENT_REDIRECT_URI_PATTERN
                .replace("{baseUrl}", securityPropertyAccess.getKeycloakClientAppBaseUri(true))
                .replace("{registrationId}", getRegistrationId());

    }

    private String getAuthorizationUri() {
        return KEYCLOAK_AUTHORIZATION_URI_PATTERN
                .replace("{providerBaseUri}", securityPropertyAccess.getKeycloakProviderBaseUri(true))
                .replace("{realmName}", securityPropertyAccess.getKeycloakProviderRealm(true));
    }


    private String getTokenUri() {
        return KEYCLOAK_TOKEN_URI_PATTERN
                .replace("{providerBaseUri}", securityPropertyAccess.getKeycloakProviderBaseUri(true))
                .replace("{realmName}", securityPropertyAccess.getKeycloakProviderRealm(true));
    }

    private String getUserInfoUri() {
        return KEYCLOAK_USER_INFO_URI_PATTERN
                .replace("{providerBaseUri}", securityPropertyAccess.getKeycloakProviderBaseUri(true))
                .replace("{realmName}", securityPropertyAccess.getKeycloakProviderRealm(true));
    }

    private String getJwksSetUri() {
        return KEYCLOAK_JWKS_URI_PATTERN
                .replace("{providerBaseUri}", securityPropertyAccess.getKeycloakProviderBaseUri(true))
                .replace("{realmName}", securityPropertyAccess.getKeycloakProviderRealm(true));
    }

    private String getEndSessionEndpoint() {
        return KEYCLOAK_END_SESSION_ENDPOINT_PATTERN
                .replace("{providerBaseUri}", securityPropertyAccess.getKeycloakProviderBaseUri(true))
                .replace("{realmName}", securityPropertyAccess.getKeycloakProviderRealm(true));
    }

    private String getRevocationEndpoint() {
        return KEYCLOAK_REVOCATION_ENDPOINT_PATTERN
                .replace("{providerBaseUri}", securityPropertyAccess.getKeycloakProviderBaseUri(true))
                .replace("{realmName}", securityPropertyAccess.getKeycloakProviderRealm(true));
    }

    private String getIntrospectionEndpoint() {
        return KEYCLOAK_INTROSPECTION_ENDPOINT_PATTERN
                .replace("{providerBaseUri}", securityPropertyAccess.getKeycloakProviderBaseUri(true))
                .replace("{realmName}", securityPropertyAccess.getKeycloakProviderRealm(true));
    }

    private String getRegistrationEndpoint() {
        return KEYCLOAK_REGISTRATION_ENDPOINT_PATTERN
                .replace("{providerBaseUri}", securityPropertyAccess.getKeycloakProviderBaseUri(true))
                .replace("{realmName}", securityPropertyAccess.getKeycloakProviderRealm(true));
    }

    private String getCheckSessionIframe() {
        return KEYCLOAK_CHECK_SESSION_IFRAME_PATTERN
                .replace("{providerBaseUri}", securityPropertyAccess.getKeycloakProviderBaseUri(true))
                .replace("{realmName}", securityPropertyAccess.getKeycloakProviderRealm(true));
    }

    private String getIssuerUri() {
        return KEYCLOAK_ISSUER_URI_PATTERN
                .replace("{providerBaseUri}", securityPropertyAccess.getKeycloakProviderBaseUri(true))
                .replace("{realmName}", securityPropertyAccess.getKeycloakProviderRealm(true));
    }

}
