package bg.duosoft.keycloak.openidsecurity.config.logout;

import bg.duosoft.keycloak.openidsecurity.exception.LogoutException;
import bg.duosoft.keycloak.openidsecurity.model.KeycloakUser;
import bg.duosoft.keycloak.openidsecurity.utils.JwtAttributes;
import bg.duosoft.keycloak.openidsecurity.utils.JwtUtils;
import com.nimbusds.jwt.JWT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Objects;

@Slf4j
public class CustomLogoutHandler {

    private static final String LOGOUT_TOKEN_PARAMETER_NAME = "logout_token";

    public static LogoutHandler getLogoutHandler(SessionRegistry sessionRegistry) {
        return (request, response, authentication) -> {
            if (request.getParameterMap().containsKey(LOGOUT_TOKEN_PARAMETER_NAME)) {
                String logoutToken = request.getParameter(LOGOUT_TOKEN_PARAMETER_NAME);
                if (Objects.nonNull(sessionRegistry)) {
                    List<Object> users = sessionRegistry.getAllPrincipals();
                    if (!CollectionUtils.isEmpty(users)) {
                        expireActiveUserSessions(sessionRegistry, logoutToken, users);
                    }
                }
            } else {
                SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
                securityContextLogoutHandler.logout(request, response, authentication);
            }
        };
    }

    private static void expireActiveUserSessions(SessionRegistry sessionRegistry, String logoutToken, List<Object> users) {
        try {
            JWT logoutJwt = JwtUtils.generateJwt(logoutToken);
            String logoutTokenSid = (String) logoutJwt.getJWTClaimsSet().getClaim(JwtAttributes.SESSION_ID);
            String logoutTokenUserId = (String) logoutJwt.getJWTClaimsSet().getClaim(JwtAttributes.USER_ID);
            for (Object user : users) {
                KeycloakUser keycloakUser = (KeycloakUser) user;
                String accessTokenSid = keycloakUser.getAttribute(JwtAttributes.SESSION_STATE);
                String accessTokenUserId = keycloakUser.getAttribute(JwtAttributes.USER_ID);
                if (StringUtils.hasText(accessTokenSid) && StringUtils.hasText(accessTokenUserId)) {
                    if (accessTokenUserId.equals(logoutTokenUserId) && accessTokenSid.equals(logoutTokenSid)) {
                        List<SessionInformation> activeSessions = sessionRegistry.getAllSessions(user, false);
                        if (!CollectionUtils.isEmpty(activeSessions)) {
                            for (SessionInformation activeSession : activeSessions) {
                                activeSession.expireNow();
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new LogoutException(e.getMessage(), e);
        }
    }

}
