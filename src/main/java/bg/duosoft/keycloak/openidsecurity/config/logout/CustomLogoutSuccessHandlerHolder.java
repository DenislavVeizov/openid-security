//package bg.duosoft.keycloak.openidsecurity.config.logout;
//
//import org.springframework.beans.BeansException;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.ApplicationContextAware;
//import org.springframework.security.oauth2.client.oidc.web.logout.OidcClientInitiatedLogoutSuccessHandler;
//import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
//import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
//
//public class CustomLogoutSuccessHandlerHolder implements ApplicationContextAware {
//
//    private ClientRegistrationRepository clientRegistrationRepository;
//
//    public static LogoutSuccessHandler getLogoutSuccessHandler() {
//        CustomLogoutSuccessHandlerHolder holder = new CustomLogoutSuccessHandlerHolder();
//        return holder.logoutSuccessHandler();
//    }
//
//    private LogoutSuccessHandler logoutSuccessHandler() {
//        OidcClientInitiatedLogoutSuccessHandler oidcLogoutSuccessHandler = new OidcClientInitiatedLogoutSuccessHandler(clientRegistrationRepository);
//        // Sets the location that the End-User's User Agent will be redirected to
//        // after the logout has been performed at the Provider
//        oidcLogoutSuccessHandler.setPostLogoutRedirectUri("{baseUrl}");
//        return oidcLogoutSuccessHandler;
//    }
//
//    @Override
//    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//        this.clientRegistrationRepository = applicationContext.getBean("clientRegistrationRepository", ClientRegistrationRepository.class);
//    }
//}
