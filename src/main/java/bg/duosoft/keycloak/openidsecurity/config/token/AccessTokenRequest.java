package bg.duosoft.keycloak.openidsecurity.config.token;

import org.springframework.core.convert.converter.Converter;
import org.springframework.http.RequestEntity;
import org.springframework.security.oauth2.client.endpoint.DefaultAuthorizationCodeTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequestEntityConverter;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.Objects;

public class AccessTokenRequest {

    private static final String CLIENT_SESSION_STATE = "client_session_state";
    private static final String CLIENT_SESSION_HOST = "client_session_host";

    public static OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> customAccessTokenResponseClient() {
        DefaultAuthorizationCodeTokenResponseClient tokenResponseClient = new DefaultAuthorizationCodeTokenResponseClient();
        tokenResponseClient.setRequestEntityConverter(new RequestEntityConverter());
        return tokenResponseClient;
    }

    private static class RequestEntityConverter implements Converter<OAuth2AuthorizationCodeGrantRequest, RequestEntity<?>> {
        private final OAuth2AuthorizationCodeGrantRequestEntityConverter defaultConverter;

        public RequestEntityConverter() {
            defaultConverter = new OAuth2AuthorizationCodeGrantRequestEntityConverter();
        }

        @Override
        public RequestEntity<?> convert(OAuth2AuthorizationCodeGrantRequest req) {
            RequestEntity<?> entity = defaultConverter.convert(req);
            MultiValueMap<String, String> params = (MultiValueMap<String, String>) entity.getBody();
            addParametersRequiredForSingleLogout(params);
            return new RequestEntity<>(params, entity.getHeaders(), entity.getMethod(), entity.getUrl());
        }

        private static void addParametersRequiredForSingleLogout(MultiValueMap<String, String> params) {
            String clientSessionState = null;
            RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
            if (Objects.nonNull(requestAttributes)) {
                String sessionId = requestAttributes.getSessionId();
                if (StringUtils.hasText(sessionId)) {
                    clientSessionState = sessionId;
                }
            }

            //Added additional parameters, because SLO depends on it !
            if (Objects.nonNull(clientSessionState)) {
                params.add(CLIENT_SESSION_STATE, clientSessionState);
//                params.add(CLIENT_SESSION_HOST, "localhost");
            }
        }
    }



}
