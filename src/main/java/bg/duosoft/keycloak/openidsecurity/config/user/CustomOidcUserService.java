package bg.duosoft.keycloak.openidsecurity.config.user;

import bg.duosoft.keycloak.openidsecurity.converter.KeycloakRoleConverter;
import bg.duosoft.keycloak.openidsecurity.model.KeycloakUser;
import bg.duosoft.keycloak.openidsecurity.utils.JwtAttributes;
import bg.duosoft.keycloak.openidsecurity.utils.JwtUtils;
import com.nimbusds.jwt.JWT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
public class CustomOidcUserService implements OAuth2UserService<OidcUserRequest, OidcUser> {

    @Override
    public KeycloakUser loadUser(OidcUserRequest oidcUserRequest) throws OAuth2AuthenticationException {
        final OidcUserService delegate = new OidcUserService();

        List<GrantedAuthority> authorities = new ArrayList<>();

        // Delegate to the default implementation for loading a user
        OidcUser oidcUser = delegate.loadUser(oidcUserRequest);
        Collection<? extends GrantedAuthority> defaultAuthorities = oidcUser.getAuthorities();
        if (!CollectionUtils.isEmpty(defaultAuthorities)) {
            authorities.addAll(defaultAuthorities);
        }

        OAuth2AccessToken accessToken = oidcUserRequest.getAccessToken();
        JWT accessJwtToken = JwtUtils.generateJwt(accessToken.getTokenValue());

        KeycloakRoleConverter roleConverter = new KeycloakRoleConverter();
        List<GrantedAuthority> mappedAuthorities = (List<GrantedAuthority>) roleConverter.convert(accessJwtToken);
        if (!CollectionUtils.isEmpty(mappedAuthorities)) {
            authorities.addAll(mappedAuthorities);
        }

        KeycloakUser result = new KeycloakUser(authorities, oidcUser.getIdToken(), oidcUser.getUserInfo());
        result.setGroups(selectGroups(accessJwtToken));
        return result;
    }

    private List<String> selectGroups(JWT accessJwtToken) {
        List<String> groups = new ArrayList<>();
        try {
            List<String> groupsClaim = (List<String>) accessJwtToken.getJWTClaimsSet().getClaim(JwtAttributes.GROUPS);
            if (!CollectionUtils.isEmpty(groupsClaim)) {
                groups.addAll(groupsClaim);
            }
        } catch (Exception e) {
            log.error("User groups are not set! Access Token:  {}" + accessJwtToken.getParsedString());
            log.error(e.getMessage(), e);
        }
        return groups;
    }
}
