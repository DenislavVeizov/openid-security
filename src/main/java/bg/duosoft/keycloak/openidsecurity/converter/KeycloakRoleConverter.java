package bg.duosoft.keycloak.openidsecurity.converter;

import bg.duosoft.keycloak.openidsecurity.exception.ReadClaimException;
import bg.duosoft.keycloak.openidsecurity.utils.JwtAttributes;
import com.nimbusds.jwt.JWT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class KeycloakRoleConverter implements Converter<JWT, Collection<GrantedAuthority>> {

    @Override
    public Collection<GrantedAuthority> convert(JWT jwt) {
        Map<String, Object> realmAccess;
        try {
            realmAccess = (Map<String, Object>) jwt.getJWTClaimsSet().getClaim(JwtAttributes.REALM_ACCESS);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ReadClaimException(JwtAttributes.REALM_ACCESS, jwt.getParsedString());
        }

        if (realmAccess == null || realmAccess.isEmpty()) {
            return new ArrayList<>();
        }

        Collection<GrantedAuthority> roles = ((List<String>) realmAccess.get(JwtAttributes.ROLES))
                .stream()
                .map(roleName -> "ROLE_" + roleName)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        return roles;
    }


}