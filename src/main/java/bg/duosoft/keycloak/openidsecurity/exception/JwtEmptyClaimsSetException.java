package bg.duosoft.keycloak.openidsecurity.exception;

public class JwtEmptyClaimsSetException extends RuntimeException {

    public JwtEmptyClaimsSetException() {
    }

    public JwtEmptyClaimsSetException(String message) {
        super(message);
    }

    public JwtEmptyClaimsSetException(String message, Throwable cause) {
        super(message, cause);
    }

    public JwtEmptyClaimsSetException(Throwable cause) {
        super(cause);
    }

    public JwtEmptyClaimsSetException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
