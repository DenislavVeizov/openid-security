package bg.duosoft.keycloak.openidsecurity.exception;


public class JwtParserException extends RuntimeException {

    public JwtParserException() {
    }

    public JwtParserException(String message) {
        super(message);
    }

    public JwtParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public JwtParserException(Throwable cause) {
        super(cause);
    }

    public JwtParserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
