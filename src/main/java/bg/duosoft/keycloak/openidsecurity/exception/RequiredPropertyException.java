package bg.duosoft.keycloak.openidsecurity.exception;

public class RequiredPropertyException extends RuntimeException {

    public RequiredPropertyException(String message) {
        super("Property value is empty! Property name: " + message);
    }
}
