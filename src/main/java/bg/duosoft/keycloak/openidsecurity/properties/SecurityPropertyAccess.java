package bg.duosoft.keycloak.openidsecurity.properties;

import bg.duosoft.keycloak.openidsecurity.exception.RequiredPropertyException;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@Getter
public class SecurityPropertyAccess {

    @Value("${keycloak.provider.base-uri}")
    private String keycloakProviderBaseUri;

    @Value("${keycloak.provider.realm}")
    private String keycloakProviderRealm;

    @Value("${keycloak.provider.load-well-known-config:#{true}}")
    private Boolean keycloakProviderLoadWellKnownConfig;

    @Value("${keycloak.client.id}")
    private String keycloakClientId;

    @Value("${keycloak.client.secret}")
    private String keycloakClientSecret;

    @Value("${keycloak.client.registration-id:#{null}}")
    private String keycloakClientRegistrationId;

    @Value("${keycloak.client.scope:#{null}}")
    private String[] keycloakClientScope;

    @Value("${keycloak.client.authorization-grant-type:#{null}}")
    private String keycloakClientAuthorizationGrantType;

    @Value("${keycloak.client.redirect-uri:#{null}}")
    private String keycloakClientRedirectUri;

    @Value("${keycloak.client.app-base-uri:#{null}}")
    private String keycloakClientAppBaseUri;

    public String getKeycloakClientAppBaseUri(boolean isRequired) {
        if (isRequired && !StringUtils.hasText(this.keycloakClientAppBaseUri)) {
            throw new RequiredPropertyException("oauth2ClientAppBaseUri");
        }
        return this.keycloakClientAppBaseUri;
    }

    public String getKeycloakClientId(boolean isRequired) {
        if (isRequired && !StringUtils.hasText(this.keycloakClientId)) {
            throw new RequiredPropertyException("oauth2ClientId");
        }
        return this.keycloakClientId;
    }


    public String getKeycloakClientSecret(boolean isRequired) {
        if (isRequired && !StringUtils.hasText(this.keycloakClientSecret)) {
            throw new RequiredPropertyException("oauth2ClientSecret");
        }
        return keycloakClientSecret;
    }

    public String getKeycloakProviderRealm(boolean isRequired) {
        if (isRequired && !StringUtils.hasText(this.keycloakProviderRealm)) {
            throw new RequiredPropertyException("oauth2ProviderRealm");
        }
        return keycloakProviderRealm;
    }

    public String getKeycloakProviderBaseUri(boolean isRequired) {
        if (isRequired && !StringUtils.hasText(this.keycloakProviderBaseUri)) {
            throw new RequiredPropertyException("oauth2ProviderBaseUri");
        }
        return keycloakProviderBaseUri;
    }
}
