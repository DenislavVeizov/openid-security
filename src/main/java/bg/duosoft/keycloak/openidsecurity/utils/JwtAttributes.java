package bg.duosoft.keycloak.openidsecurity.utils;

public class JwtAttributes {

    public static final String SESSION_STATE = "session_state";
    public static final String SESSION_ID = "sid";
    public static final String USER_ID = "sub";
    public static final String GROUPS = "groups";
    public static final String REALM_ACCESS = "realm_access";
    public static final String ROLES = "roles";


}
