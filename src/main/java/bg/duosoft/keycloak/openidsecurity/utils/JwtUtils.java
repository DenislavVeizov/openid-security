package bg.duosoft.keycloak.openidsecurity.utils;

import bg.duosoft.keycloak.openidsecurity.exception.JwtEmptyClaimsSetException;
import bg.duosoft.keycloak.openidsecurity.exception.JwtParserException;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.SignedJWT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.Objects;

@Slf4j
public class JwtUtils {

    public static JWT generateJwt(String token) {
        try {
            String[] split_string = token.split("\\.");
            String base64EncodedHeader = split_string[0];
            String base64EncodedBody = split_string[1];
            String base64EncodedSignature = split_string[2];

            SignedJWT jwt = new SignedJWT(Base64URL.from(base64EncodedHeader), Base64URL.from(base64EncodedBody), Base64URL.from(base64EncodedSignature));
            if (Objects.isNull(jwt.getJWTClaimsSet())) {
                throw new JwtEmptyClaimsSetException("Jwt claim set is empty! Token: " + token);
            }
            return jwt;
        } catch (Exception e) {
            throw new JwtParserException("Token: " + token + "\n" + e.getMessage(), e);
        }
    }

}
