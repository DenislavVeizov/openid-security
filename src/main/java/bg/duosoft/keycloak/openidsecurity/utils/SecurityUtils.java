package bg.duosoft.keycloak.openidsecurity.utils;

import bg.duosoft.keycloak.openidsecurity.model.KeycloakUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class SecurityUtils {
    public static boolean isUserLoggedIn() {
        return !Objects.isNull(getLoggedUser());
    }

    public static KeycloakUser getLoggedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (Objects.isNull(auth) || auth.getName().equalsIgnoreCase("anonymousUser"))
            return null;

        return (KeycloakUser) auth.getPrincipal();
    }

    public static String getLoggedUsername() {
        KeycloakUser loggedUser = getLoggedUser();
        if (Objects.isNull(loggedUser))
            return null;

        return loggedUser.getPreferredUsername();
    }

    public static String getLoggedUserFullName() {
        KeycloakUser loggedUser = getLoggedUser();
        if (Objects.isNull(loggedUser))
            return null;

        return loggedUser.getFullName();
    }

    public static String getLoggedUserId() {
        KeycloakUser loggedUser = getLoggedUser();
        if (Objects.isNull(loggedUser))
            return null;

        return loggedUser.getUserInfo().getSubject();
    }

    public static List<String> getLoggedUserGroups() {
        KeycloakUser loggedUser = getLoggedUser();
        if (Objects.isNull(loggedUser))
            return null;

        Collection<? extends GrantedAuthority> authorities = loggedUser.getAuthorities();

        return loggedUser.getGroups();
    }

    public static List<GrantedAuthority> getLoggedUserAuthorities() {
        KeycloakUser loggedUser = getLoggedUser();
        if (Objects.isNull(loggedUser))
            return null;

        return (List<GrantedAuthority>) loggedUser.getAuthorities();
    }


}
